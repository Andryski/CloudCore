#FROM golang:latest
#ENV CGO_ENABLED 0
#RUN mkdir /app
#ADD . /app/
#WORKDIR /app
#RUN go build -o main .
#CMD ["/app/main"]

FROM golang:1.14.2-alpine3.11 AS build-env
ENV CGO_ENABLED 0

# Compile Delve
RUN apk add --no-cache git
RUN go get github.com/derekparker/delve/cmd/dlv

ADD ./src /go/src/cloudCore/src

#RUN go get /app
#WORKDIR /app
# The -gcflags "all=-N -l" flag helps us get a better debug experience
#RUN go build -gcflags "all=-N -l" -o /server hello

RUN go get cloudCore/src
RUN go build -gcflags "all=-N -l" -o /main cloudCore/src

# Final stage
FROM alpine:3.11
# Port 8080 belongs to our application, 40000 belongs to Delve
EXPOSE 8088 40000
# Allow delve to run on Alpine based containers.
RUN apk add --no-cache libc6-compat
WORKDIR /

COPY --from=build-env /main /
COPY --from=build-env /go/bin/dlv /

# Run delve
CMD ["/dlv", "--listen=:40000", "--headless=true", "--api-version=2", "exec", "/main"]
#CMD ["/servermy"]